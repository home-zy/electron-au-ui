import { createStore } from 'vuex'
// import createPersistedState from "vuex-persistedstate"
// import auIndexDB from "../utils/au.indexdb.js"

import config from "@/store/config";
// const Store = require('electron-store');
// window.store = window.store||new Store();
// const configKey="auConfigVuexKey";

export default createStore({
  modules: {
    config:config
  },
  // plugins: [createPersistedState({})]
})
