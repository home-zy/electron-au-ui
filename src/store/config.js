
export default {
    namespaced: true,
    state(){
        return {
            name:"YouYou助手",
            icon:require("@/assets/icon.jpg"),
            buttonConfig:{
                max:false,
                min:true,
                setting:true,
                close:true,
            },
            showSetting:false
        }
    },
    mutations: {
        set(state,{key,value}){
            console.log(key,value)
            state[key]=value
        }
    },
    actions: {
    }
}
