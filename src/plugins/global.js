import config from "@/utils/au.config";
import auMedia from "@/utils/au.media";
import auKey from "@/utils/au.key";

export default (app) => {
    window.auMedia=auMedia;
    window.auKey=auKey;
    app.config.globalProperties.$config=config
    app.config.globalProperties.$auKey=auKey
}
