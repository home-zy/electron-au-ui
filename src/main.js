import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import "./style/app.scss"
import installElementPlus from './plugins/element'
import installAppGlobal from './plugins/global'

const app = createApp(App)
installElementPlus(app)
installAppGlobal(app)
app.use(store).mount('#app')
