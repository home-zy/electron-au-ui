import auDB from"./au.indexdb"

const configKey="auConfigKey"
export default {
    get(key){
        return auDB(configKey).findDataByKey(key).then(x=>x.value)
    },
    set(key,value){
        return auDB(configKey).put({id:key,value})
    },
    delete(key){
        return auDB(configKey).delete(key)
    }
}
